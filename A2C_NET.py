# [Yunqiu Xu] A2C_NET
# A2CPolicy_RAW: plain A2C

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
from utils import orthogonal


# Helper function : init
def weights_init(m):
    """Orthogonal initialization"""
    classname = m.__class__.__name__
    if classname.find('Conv') != -1 or classname.find('Linear') != -1:
        orthogonal(m.weight.data)
        if m.bias is not None:
            m.bias.data.fill_(0)

class A2CPolicy_RAW(nn.Module):
    """ Raw A2C """
    def __init__(self, num_inputs, num_actions):
        super(A2CPolicy_RAW, self).__init__()
        # Basic layers
        self.conv1 = nn.Conv2d(num_inputs, 32, 8, stride=4)
        self.conv2 = nn.Conv2d(32, 64, 4, 2)
        self.conv3 = nn.Conv2d(64, 64, 3, 1)
        self.fc1 = nn.Linear(64 * 7 * 7, 512)
        # Actor-Critic
        self.critic_linear = nn.Linear(512, 1)
        self.actor_linear = nn.Linear(512, num_actions)
        # training mode, parameters initialization
        self.train()
        self.reset_parameters()

    def reset_parameters(self):
        """Initialize weight
        For all layers use orthogonal initialization
        For all layers except critic layer and action layer, apply relu_gain
        """
        self.apply(weights_init)
        relu_gain = nn.init.calculate_gain('relu')
        self.conv1.weight.data.mul_(relu_gain)
        self.conv2.weight.data.mul_(relu_gain)
        self.conv3.weight.data.mul_(relu_gain)
        self.fc1.weight.data.mul_(relu_gain)

    def forward(self, inputs):
        """ forward pass
        :param inputs: current observations, (16, 4, 84, 84)
        :output values: critic
        :output logits: actor
        """ 
        x = F.relu(self.conv1(inputs / 255.0))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = x.view(-1, 64 * 7 * 7)
        x = F.relu(self.fc1(x))
        values = self.critic_linear(x)
        logits = self.actor_linear(x)
        return values, logits
    
    def get_action(self, inputs):
        """ choose action
        :param inputs: current observations
        :output values:
        :output actions:
        :output action_log_probs:
        """
        # Forward pass to get values and logits
        values, logits = self.forward(inputs)
        # Get distribution
        dist = torch.distributions.Categorical(logits=logits)
        # Sample actions
        actions = dist.sample().view(-1, 1)
        # Compute action log probs
        log_probs = F.log_softmax(logits, dim=1)
        action_log_probs = log_probs.gather(1, actions)
        return values, actions, action_log_probs

    def evaluate_actions(self, inputs, actions):
        """evaluate action
        :param inputs: current observations
        :param actions: choosed actions
        :output values:
        :output action_log_probs:
        :output dist_entropy:
        """
        # Forward pass to get values and logits
        values, logits = self.forward(inputs)
        # Get distribution
        dist = torch.distributions.Categorical(logits=logits)
        # Compute action_log_probs
        log_probs = F.log_softmax(logits, dim=1)
        action_log_probs = log_probs.gather(1, actions)
        # Compute dist entropy
        dist_entropy = dist.entropy().mean()
        return values, action_log_probs, dist_entropy

if __name__ == "__main__":
    # Build input, note that here the sample has been divided by 255.
    sample_input = torch.randint(low=0, high=255, size=(5, 4, 84, 84)).float() / 255.
    print("Obs : {}".format(sample_input.shape))
    print("=====")
    # Actor critic
    print("Test actor critic:")
    a2c_raw = A2CPolicy_RAW(num_inputs=4, num_actions=18)
    print(a2c_raw)
    print("-----")
    print("forward")
    values, logits = a2c_raw.forward(inputs=sample_input)
    print("values : {}".format(values.shape))
    print("logits : {}".format(logits.shape))
    print("-----")
    print("get_action")
    values, actions, action_log_probs = a2c_raw.get_action(inputs=sample_input)
    print("values : {}".format(values.shape))
    print("actions : {}".format(actions.shape))
    print("action_log_probs : {}".format(action_log_probs.shape))
    print("=====")
