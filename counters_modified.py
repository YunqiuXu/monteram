# [Yunqiu Xu] Modified counters
# In this work there are 2 counting parts: classifying the state (room) and evaluating the novelty of state (intrinsic reward)
# Refer Counters.ipynb for more details
# Diffeences from prev one: in OBS_Encoder, replace "CNNs" with "flatten -> FC"

import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from utils import orthogonal

def weights_init(m):
    """Orthogonal initialization"""
    classname = m.__class__.__name__
    if classname.find('Conv') != -1 or classname.find('Linear') != -1:
        orthogonal(m.weight.data)
        if m.bias is not None:
            m.bias.data.fill_(0)

class OBS_Encoder(nn.Module):
    """ Note that this encoder is not learnable, just for encoding the obs """
    def __init__(self, num_channels=3):
        super(OBS_Encoder, self).__init__()
        self.fc1 = nn.Linear(84 * 84 * num_channels, 64)
        self.reset_parameters()

    def reset_parameters(self):
        """Initialize weight"""
        self.apply(weights_init)
    
    def forward(self, inputs):
        # flatten
        batch_size = inputs.shape[0]
        x = (inputs / 255.0).view(batch_size, -1)
        x = self.fc1(x)
        return x

class OBS_Counter(object):
    def __init__(self, num_channels = 3,tau = 0.7,use_cuda=False):
        # obs encoder
        self.obs_encoder = OBS_Encoder(num_channels)
        if use_cuda:
            self.obs_encoder.cuda()
        # key: room index, value: room embedding
        self.embedding_collecter = {}
        # key: room index, value: number of obs belong to this room
        self.embedding_counter = {}
        # the index of next new room
        self.next_index = 0
        # tau: the distance threshold, None for those with no room change
        self.threshold = tau
        
    def get_room_class(self, single_embedding):
        """
        get the class of single embedding
        :param single_embedding: torch.Size([64])
        """
        room_class = None
        for curr_room_class in self.embedding_collecter.keys():
            curr_room_embedding = self.embedding_collecter[curr_room_class]
            distance = torch.sqrt(torch.sum(torch.pow(single_embedding - curr_room_embedding, 2)))
            if distance.item() < self.threshold:
                room_class = curr_room_class
                return room_class
        return room_class
    
    def update_room(self, single_embedding, room_class):
        """
        :param single_embedding: [64,] cuda enable
        """
        if room_class in self.embedding_collecter.keys():
            # get curr embedding and counting
            curr_room_embedding = self.embedding_collecter[room_class]
            curr_room_counting = self.embedding_counter[room_class]
            # assign new embedding and counting
            new_room_embedding = (single_embedding + (curr_room_counting * curr_room_embedding)) / (curr_room_counting + 1)
            self.embedding_collecter[room_class] = new_room_embedding
            self.embedding_counter[room_class] = curr_room_counting + 1
        else:
            self.embedding_collecter[room_class] = single_embedding
            self.embedding_counter[room_class] = 1

    def get_room_class_batch(self, obs_next):
        """
        :param obs_next: [batch, 1, 84, 84], cuda enable
        :output room_classes: [batch, 1], dtype = int64
        """
        embeddings = self.obs_encoder.forward(obs_next)
        room_classes = []
        for curr_embedding in embeddings:
            # If no threshold, just set the class as 1
            if self.threshold is not None:
                room_class = self.get_room_class(curr_embedding)
            else:
                room_class = 1
            # For new room ,assign new class
            if room_class is None:
                self.next_index += 1
                room_class = self.next_index
            room_classes.append(room_class)
            self.update_room(curr_embedding, room_class)
        room_classes = torch.from_numpy(np.array(room_classes)).long()
        room_classes = room_classes.view(-1,1)
        if torch.cuda.is_available():
            room_classes = room_classes.cuda()
        return room_classes


class StateVisitationCounter(object):
    def __init__(self):
        self.counter = {}

    def insert(self, representation):
        """
        Here representation is a 4-item-tuple, where each item is an integer
        (x,y,c,R)
        """
        if representation in self.counter:
            # print("Old representation {}".format(representation))
            self.counter[representation] += 1
        else:
            self.counter[representation] = 1

    def get_intrinsic_reward(self, representation_batch):
        """
        :param representation_batch: [batch, 4], dtype int64 (long)
        :output intrinsic_reward_batch: [batch, 1], dtype float
        """
        intrinsic_reward_batch = []
        for curr_representation in representation_batch:
            # build tuple
            curr_representation_tuple = tuple(curr_representation.cpu().numpy())
            # insert tuple into counter
            self.insert(curr_representation_tuple)
            # compute intrinsic reward
            curr_intrinsic_reward = 1. / np.sqrt(self.counter[curr_representation_tuple])
            intrinsic_reward_batch.append(curr_intrinsic_reward)
        # build output tensor
        intrinsic_reward_batch = torch.from_numpy(np.array(intrinsic_reward_batch)).float()
        intrinsic_reward_batch = intrinsic_reward_batch.view(-1,1)
        if torch.cuda.is_available():
            intrinsic_reward_batch = intrinsic_reward_batch.cuda()
        return intrinsic_reward_batch
