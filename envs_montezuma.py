# [Yunqiu Xu] Modify env to get access to RAM
# For wrap_deepmind(env)
#   episode_life=False: the game should done after losing all lives
#   clip_rewards=False: reward clipping shoud be done after computing intrinsic reward
# The single obs is like [batch,1,85,84]
#   If there are 4 stacked obs, the shape is like [batch,4,85,84]
#   For a2c, the input should be [:,:,:84,:]
#   The controllable position is [:,:,84,:2]

import os
import gym
import numpy as np
import torch
from gym.spaces.box import Box
from baselines import bench
from baselines.common.atari_wrappers import make_atari, wrap_deepmind
from baselines.common.vec_env import VecEnvWrapper
from baselines.common.vec_env.dummy_vec_env import DummyVecEnv
from baselines.common.vec_env.subproc_vec_env import SubprocVecEnv
from baselines.common.vec_env.vec_normalize import VecNormalize
import cv2

# Main function
def make_vec_envs(env_name, seed, num_processes, log_dir, add_timestep, device, allow_early_resets):
    envs = [make_env(env_name, seed, i, log_dir, add_timestep, allow_early_resets)
                for i in range(num_processes)]
    if len(envs) > 1:
        envs = SubprocVecEnv(envs)
    else:
        envs = DummyVecEnv(envs)
    envs = VecPyTorch(envs, device)
    return envs

# [Yunqiu Xu] simplify the function, for wrap_deepmind, set "episode_life=False, clip_rewards=False"
def make_env(env_id, seed, rank, log_dir, add_timestep, allow_early_resets):
    def _thunk():
        env = make_atari(env_id)
        env.seed(seed + rank)
        if log_dir is not None:
            env = bench.Monitor(env, os.path.join(log_dir, str(rank)), allow_early_resets=allow_early_resets)
        env = wrap_deepmind(env, episode_life=False, clip_rewards=False)
        env = TransposeImage(env)
        return env
    return _thunk

# [Yunqiu Xu]: change observation shape
class TransposeImage(gym.ObservationWrapper):
    def __init__(self, env=None):
        super(TransposeImage, self).__init__(env)
    def observation(self, observation):
        """ 
        :param observation: (84,84,1), uint8
        :output obs_combined: (1,85,84), add one row to store (x_coordinator and y_coordinator)
        """
        # get (x, y) range in (9,9)
        curr_ram_value = self.unwrapped.ale.getRAM()
        # [Yunqiu Xu] need to check "int()" or "round()"
        x_coordinate = round((curr_ram_value[42]) * 9. / 160)
        y_coordinate = round((320 - curr_ram_value[43]) * 9. / 210)
        # print(x_coordinate, y_coordinate)
        obs_combined = np.zeros((85,84,1)).astype(np.uint8)
        obs_combined[:84,:84,0:1] = observation
        obs_combined[84,0,0] = x_coordinate
        obs_combined[84,1,0] = y_coordinate
        obs_combined = obs_combined.transpose(2, 0, 1) # (1,85,84)
        return obs_combined

class VecPyTorch(VecEnvWrapper):
    """ https://github.com/ikostrikov/pytorch-a2c-ppo-acktr/blob/master/a2c_ppo_acktr/envs.py """
    def __init__(self, venv, device):
        """Return only every `skip`-th frame"""
        super(VecPyTorch, self).__init__(venv)
        self.device = device
        # TODO: Fix data types
    def reset(self):
        obs = self.venv.reset()
        obs = torch.from_numpy(obs).float().to(self.device)
        return obs
    def step_async(self, actions):
        actions = actions.squeeze(1).cpu().numpy()
        self.venv.step_async(actions)
    def step_wait(self):
        obs, reward, done, info = self.venv.step_wait()
        obs = torch.from_numpy(obs).float().to(self.device)
        reward = torch.from_numpy(np.expand_dims(np.stack(reward), 1)).float()
        return obs, reward, done, info


if __name__ == "__main__":
    print("Test")
    envs = make_vec_envs('MontezumaRevengeNoFrameskip-v4', 
                        1, 
                        2,
                        'log', 
                        False, 
                        torch.device("cpu"), 
                        False)
    print("Envs loaded successfully")
    obs = envs.reset()
    print("obs : {}".format(obs.shape))
    print("controllable positions : {}".format(obs[:,:,84,:2]))
    print("All finished")
