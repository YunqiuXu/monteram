# [Yunqiu Xu] Hope this be the final version
# Replace ADM with RAM to check the upperbound of algorithm
# Modify the method to collect R (episodicly) to build (x,y,c,R)

import gym
import numpy as np
import cv2
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import copy
from baselines.common.vec_env.dummy_vec_env import DummyVecEnv
from baselines.common.vec_env.subproc_vec_env import SubprocVecEnv
from collections import deque
# customized modules
from envs_montezuma import make_vec_envs
from A2C_NET import A2CPolicy_RAW
from A2C_rollouts import RolloutStorage
from counters_modified import OBS_Counter, StateVisitationCounter

# Set hyper parameters
class args:
    def __init__(self):
        # For env
        self.env_name='MontezumaRevengeNoFrameskip-v4'
        self.seed=1
        self.add_timestep=False
        # General
        self.cuda=torch.cuda.is_available()
        self.num_stack=4
        self.num_steps=5
        self.num_processes=16
        self.save_model=True
        self.save_interval=10000  # save model per 10000 updates, or 10000 * 16 * 5 steps
        self.log_interval=10
        self.num_frames=100000000 # 100m steps (400m frames)
        self.log_dir='log'
        self.save_dir='saved_models'
        self.curr_run = 1
        # A2C
        self.lr=7e-4               # rmsprop
        self.gamma=.99             # env, returns
        self.eps=1e-5              # rmsprop
        self.alpha=.99             # rmsprop
        self.max_grad_norm=.5      # clip gradient
        self.value_loss_coef=.5    # loss
        self.entropy_coef=.01      # loss
        # ADM
        self.beta1=10              # coef for extrinsic reward
        self.beta2=10              # coef for intrinsic reward
        # Counter
        self.tau=0.7
args = args()

# set paths
A2C_LOG_PATH = "{}/a2c_adm_ram_final_{}_{}.txt".format(args.log_dir, args.env_name, args.curr_run)
A2C_SAVE_PATH = "{}/a2c_adm_ram_{}".format(args.save_dir, args.env_name)
print("A2C log : {}".format(A2C_LOG_PATH))
print("A2C save : {}".format(A2C_SAVE_PATH))
print("-----")

# Set num_updates
num_updates = int(args.num_frames / args.num_steps / args.num_processes) # 1250000 rollouts
print("Num updates : {}".format(num_updates))
print("-----")

# Set seed
torch.manual_seed(args.seed)
if args.cuda:
    print("Using cuda")
    torch.cuda.manual_seed(args.seed)
else:
    print("Using cpu")
print("-----")

# Main
if __name__ == "__main__":
    # Build envs
    print("Building envs")
    device = torch.device("cuda:0" if args.cuda else "cpu")
    envs = make_vec_envs(args.env_name, 
                        args.seed, 
                        args.num_processes,
                        args.log_dir, 
                        args.add_timestep, 
                        device, 
                        False)
    num_actions = envs.action_space.n
    print("-----")
    # set obs shape for stack
    obs_shape = (1,85,84)
    obs_shape = (obs_shape[0] * args.num_stack, *obs_shape[1:])
    print("Stacked obs : {}".format(obs_shape)) # [4,85,84]
    print("-----")

    # Build model and optimizer
    print("Building A2C model, the input channel of a2c is 4 (num_stack)")
    actor_critic = A2CPolicy_RAW(num_inputs=args.num_stack, num_actions=num_actions)
    if args.cuda:
        actor_critic.cuda()
    print(actor_critic)
    a2c_optimizer = optim.RMSprop(actor_critic.parameters(), 
                              args.lr, 
                              eps=args.eps, 
                              alpha=args.alpha)
    print("-----")

    # [Yunqiu Xu] Build counters, here is different from paper (84,84,3), but it seems good
    print("Building OBS counter, the input channel is 1")
    obs_counter = OBS_Counter(num_channels=1, tau=args.tau, use_cuda=args.cuda)
    print("-----")
    print("Building StateVisitationCounter")
    state_visitation_counter = StateVisitationCounter()
    print("-----")

    # Build and init rollouts
    print("Building rollouts")
    rollouts = RolloutStorage(args.num_steps, args.num_processes, obs_shape)
    current_obs = torch.zeros(args.num_processes, *obs_shape)
    print(current_obs.shape) # (batch,4,85,84)
    def update_current_obs(obs):
        shape_dim0 = 1
        if args.num_stack > 1:
            current_obs[:, :-shape_dim0] = current_obs[:, shape_dim0:]
        current_obs[:, -shape_dim0:] = obs
    obs = envs.reset()
    update_current_obs(obs)
    print(obs.shape) # (batch,1,85,84)
    rollouts.observations[0].copy_(current_obs)
    if args.cuda:
        current_obs = current_obs.cuda()
        rollouts.cuda()
    print("-----")

    print("Start training")
    episode_rewards = deque(maxlen=40)
    final_rewards = torch.zeros([args.num_processes, 1])
    # [Yunqiu Xu] Computing R for intrinsic reward, renew if done
    cumulative_reward_collector = torch.zeros([args.num_processes, 1])
    if args.cuda:
        cumulative_reward_collector = cumulative_reward_collector.cuda()
    for j in range(num_updates):
        # Collect rollout
        for step in range(args.num_steps):
            # Get action using a2c
            with torch.no_grad():
                curr_inputs = rollouts.observations[step] # (batch,4,85,84)
                a2c_inputs = curr_inputs[:,:,:84,:]  # (16,4,84,84)
                values, actions, action_log_probs = actor_critic.get_action(a2c_inputs)
            # Observe reward and next obs
            obs, rewards, dones, infos = envs.step(actions)
            # For log
            for info in infos:
                if 'episode' in info.keys():
                    episode_rewards.append(info['episode']['r'])
            # Compute intrinsic reward
            with torch.no_grad():
                # Get controllable position: (16,2) int64
                controllable_position = obs[:,:,84,:2].squeeze(1).long()
                # Get room class for obs_t+1: (16,1,84,84) -> (16,1) int64
                obs_next_a2c = obs[:,:,:84,:]
                room_class = obs_counter.get_room_class_batch(obs_next_a2c)
                # Get cumulative_reward (16,1) int64
                if args.cuda:
                    cumulative_reward_collector += rewards.cuda()
                else:
                    cumulative_reward_collector += rewards
                cumulative_reward_collector_floor = cumulative_reward_collector.long()
                # Build representation_batch: (16,4) int64
                representation_batch = torch.cat([controllable_position, room_class, cumulative_reward_collector_floor], 1)
                # Get intrinsic reward: (16,1) float
                intrinsic_rewards = state_visitation_counter.get_intrinsic_reward(representation_batch)
            # Flip extrinsic_reward
            extrinsic_rewards = torch.sign(rewards)
            # Compute total_reward
            total_rewards = args.beta1 * extrinsic_rewards + args.beta2 * intrinsic_rewards.cpu()
            # Compute masks, note that done only when all lives have lost
            masks = torch.FloatTensor([[0.0] if done_ else [1.0] for done_ in dones])
            if args.cuda:
                masks = masks.cuda()
            # Update episode reward collector
            cumulative_reward_collector *= masks
            # Update current obs, note that masks should be (16,1) -> (16,1,1,1)
            current_obs *= masks.unsqueeze(2).unsqueeze(2)
            update_current_obs(obs)
            # Insert to rollout
            rollouts.insert(step, current_obs, actions.data, action_log_probs.data, values.data, total_rewards, masks)
        # Compute next value
        with torch.no_grad():
            next_value_inputs = rollouts.observations[-1]
            next_value_a2c_inputs = next_value_inputs[:,:,:84,:]
            next_value = actor_critic.forward(next_value_a2c_inputs)[0].data

        # Compute returns
        rollouts.compute_returns(next_value, args.gamma)
        # Evaluate actions and compute a2c loss 
        update_obs = rollouts.observations[:-1].view(-1, *obs_shape) # (5*batch,4,85,84)
        update_obs_a2c = Variable(update_obs[:,:,:84,:])             # (5*16,85,84)
        update_actions = Variable(rollouts.actions.view(-1, 1))
        values, action_log_probs, dist_entropy = actor_critic.evaluate_actions(update_obs_a2c, update_actions)
        values = values.view(args.num_steps, args.num_processes, 1)
        action_log_probs = action_log_probs.view(args.num_steps, args.num_processes, 1)
        advantages = Variable(rollouts.returns[:-1]) - values
        value_loss = advantages.pow(2).mean()
        action_loss = -(Variable(advantages.data) * action_log_probs).mean()
        total_loss_a2c = value_loss * args.value_loss_coef + action_loss - dist_entropy * args.entropy_coef
        # Update a2c model
        a2c_optimizer.zero_grad()
        total_loss_a2c.backward()
        nn.utils.clip_grad_norm_(actor_critic.parameters(), args.max_grad_norm)
        a2c_optimizer.step()
        rollouts.after_update()
        # Save model
        if (j+1) % args.save_interval == 0 and args.save_model:
            a2c_model = actor_critic
            if args.cuda:
                a2c_model = copy.deepcopy(actor_critic).cpu()
            A2C_SAVE_PATH_curr = "{}_{}updates.pt".format(A2C_SAVE_PATH, j+1)
            torch.save(a2c_model.state_dict(), A2C_SAVE_PATH_curr)
            print("Save model : {}".format(A2C_SAVE_PATH_curr))
            print("-----")
        # Visualize
        total_num_steps = (j+1) * args.num_processes * args.num_steps
        if (j+1) % args.log_interval == 0 and len(episode_rewards) > 1:
            curr_a2c_result = "A2C Updates {}/{}|TotalSteps {}|MeanR {:.1f}|MedianR {:.1f}|MinR {:.1f}|MaxR {:.1f}|TotalLoss {:.5f}|ValueLoss {:.5f}|PolicyLoss {:.5f}|Rooms {}".format(
                    j+1, num_updates, total_num_steps,
                    np.mean(episode_rewards), np.median(episode_rewards), np.min(episode_rewards), np.max(episode_rewards),
                    total_loss_a2c.item(), value_loss.item(), action_loss.item(),
                    obs_counter.next_index)
            print(curr_a2c_result)
            with open(A2C_LOG_PATH, 'a') as f:
                f.write(curr_a2c_result + "\n")
    print("=====")
    print("Training finished")
    envs.close()
