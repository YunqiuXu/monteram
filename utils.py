# [Yunqiu Xu] provide init methods
import torch
import torch.nn as nn

# A temporary solution from the master branch.
# https://github.com/pytorch/pytorch/blob/7752fe5d4e50052b3b0bbc9109e599f8157febc0/torch/nn/init.py#L312
# Remove after the next version of PyTorch gets release.
def orthogonal(tensor, gain=1):
    if tensor.ndimension() < 2:
        raise ValueError("Only tensors with 2 or more dimensions are supported")
    rows = tensor.size(0)
    cols = tensor[0].numel()
    flattened = torch.Tensor(rows, cols).normal_(0, 1)
    if rows < cols:
        flattened.t_()
    # Compute the qr factorization
    q, r = torch.qr(flattened)
    # Make Q uniform according to https://arxiv.org/pdf/math-ph/0609050.pdf
    d = torch.diag(r, 0)
    ph = d.sign()
    q *= ph.expand_as(q)
    if rows < cols:
        q.t_()
    tensor.view_as(q).copy_(q)
    tensor.mul_(gain)
    return tensor


# Helper function : sparse max
class Sparsemax(nn.Module):
    """Sparsemax function."""
    def __init__(self, dim=None):
        """Initialize sparsemax activation
        Args:
            dim (int, optional): The dimension over which to apply the sparsemax function.
        """
        super(Sparsemax, self).__init__()
        self.dim = -1 if dim is None else dim
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    def forward(self, inputs):
        """Forward function.
        Args:
            input (torch.Tensor): Input tensor. First dimension should be the batch size
        Returns:
            torch.Tensor: [batch_size x number_of_logits] Output tensor
        """
        original_size = inputs.size()
        inputs = inputs.view(-1, inputs.size(self.dim))
        dim = 1
        number_of_logits = inputs.size(dim)
        inputs = inputs - torch.max(inputs, dim=dim, keepdim=True)[0].expand_as(inputs)
        # Sort input in descending order.
        # (NOTE: Can be replaced with linear time selection method described here:
        # http://stanford.edu/~jduchi/projects/DuchiShSiCh08.html)
        zs = torch.sort(input=inputs, dim=dim, descending=True)[0]
        ranges = torch.arange(start=1, end=number_of_logits+1, step=1., device=self.device).view(1, -1)
        ranges = ranges.expand_as(zs)
        # Determine sparsity of projection
        bound = 1 + ranges * zs
        cumulative_sum_zs = torch.cumsum(zs, dim)
        is_gt = torch.gt(bound, cumulative_sum_zs).type(inputs.type())
        k = torch.max(is_gt * ranges, dim, keepdim=True)[0]
        # Compute threshold function
        zs_sparse = is_gt * zs
        # Compute taus
        taus = (torch.sum(zs_sparse, dim, keepdim=True) - 1) / k
        taus = taus.expand_as(inputs)
        # Sparsemax
        self.output = torch.max(torch.zeros_like(inputs), inputs - taus)
        output = self.output.view(original_size)
        return output
    def backward(self, grad_output):
        """Backward function."""
        dim = 1
        nonzeros = torch.ne(self.output, 0)
        sums = torch.sum(grad_output * nonzeros, dim=dim) / torch.sum(nonzeros, dim=dim)
        self.grad_input = nonzeros * (grad_output - sums.expand_as(grad_output))
        return self.grad_input


if __name__ == "__main__":
    print("Test sparse max")
    sparsemax = Sparsemax(dim=1)
    softmax = torch.nn.Softmax(dim=1)
    logits = torch.randn(2,5)
    print("logits : \n {}".format(logits))
    print("-----")
    softmax_prob = softmax(logits)
    print("softmax prob : \n {}".format(softmax_prob))
    print("-----")
    sparsemax_prob = sparsemax(logits)
    print("sparsemax prob : \n {}".format(sparsemax_prob))
    print("-----")
